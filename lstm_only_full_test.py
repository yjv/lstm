import os.path
from os import linesep
from numpy import mean
from numpy import std
from numpy import dstack
from numpy import swapaxes
from numpy import subtract
from pandas import read_csv
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers import LSTM
from keras.utils import to_categorical
from matplotlib import pyplot

reliability, sensitivity, always = 0, 0, 0

def load_file(filepath):

  if os.path.isfile(filepath):
    # read your files
    dataframe = read_csv(filepath, header=None, delim_whitespace=False)
    return dataframe.values
  else:
    return []

def current_set():
  global reliability, sensitivity, always;
  return '_r' + str(reliability) + '_s' + str(sensitivity) + '_a' + str(always);

def load_size_with_attempt(size, attempt):
  prefix = '../logs_data/sample' + str(size).zfill(3) + current_set() + '/'
  loadedX = list()
  loadedy = list()
  for i in range(size * 2):
    for sensor in range(1, 6):
      filenameX = 'sensor' + str(sensor) + '_size_' + str(size).zfill(3) + '_attempt_' + str(attempt).zfill(3) + '_' + str(i + 1).zfill(3) + '.logs'
      dataX = load_file(prefix + filenameX)
      if (len(dataX)):
        loadedX.append(dataX)
        loadedy.append(sensor)
  return loadedX, loadedy

def load_size_with_all_attempt(size):
  loadedX = list()
  loadedy = list()
  prefix = '../logs_data/sample' + str(size).zfill(3) + current_set() + '/'
  for attempt in range(1, 11):
    for i in range(size * 2):
      for sensor in range(1, 5):
        filenameX = 'sensor' + str(sensor) + '_size_' + str(size).zfill(3) + '_attempt_' + str(attempt).zfill(3) + '_' + str(i + 1).zfill(3) + '.logs'
        dataX = load_file(prefix + filenameX)
        if (len(dataX)):
          loadedX.append(dataX)
          loadedy.append(sensor)
  return loadedX, loadedy

def prepare_data(X, y):
  X = swapaxes(X, 0, 0)
  y = to_categorical(subtract(y, 1), num_classes=5)
  return X, y

def load_dataset():
  loaded_X, loaded_y = load_size_with_attempt(250, 1)
  train_X, train_y = prepare_data(loaded_X, loaded_y)

  loaded_test_X, loaded_test_y = load_size_with_attempt(100, 1)
  test_X, test_y = prepare_data(loaded_test_X, loaded_test_y)

  return train_X, train_y, test_X, test_y

def evaluate_model(r, train_X, train_y, test_X, test_y):
  verbose, epochs, batch_size, validation_split = 0, 3, 64, 0.2
  # n_timesteps, n_features, n_outputs = train_X.shape[1], train_X.shape[2], train_y.shape[1]
  n_timesteps, n_features, n_outputs = 50, 9, 5
  model = Sequential()
  model.add(LSTM(100, input_shape=(n_timesteps,n_features)))
  model.add(Dense(n_outputs, activation='softmax'))
  model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
  # fit network
  model.fit(train_X, train_y, validation_split=validation_split, epochs=epochs, batch_size=batch_size, verbose=verbose)

  # serialize model to YAML
  model_yaml = model.to_yaml()
  with open("outputs/model" + str(r + 1) + ".yaml", "w") as yaml_file:
      yaml_file.write(model_yaml)
  # serialize weights to HDF5
  model.save_weights("outputs/model" + str(r + 1) + ".h5")

  # evaluate model
  _, accuracy = model.evaluate(test_X, test_y, batch_size=batch_size, verbose=verbose)
  return accuracy

def summarize_results(scores):
  print(scores)
  m, s = mean(scores), std(scores)
  print('Accuracy: %.3f%% (+/-%.3f)' % (m, s))

logs = list()
def run_experiment(repeats=1):
  # load data
  train_X, train_y, test_X, test_y = load_dataset()
  # repeat experiment
  scores = list()
  global logs
  for r in range(repeats):
    score = evaluate_model(r, train_X, train_y, test_X, test_y)
    score = score * 100.0
    print('set' + current_set() + '#%d: %.3f' % (r+1, score))
    scores.append(score)
    logs.append(','.join([str(reliability), str(sensitivity), str(always), '%.3f' % score]))
  # summarize results
  summarize_results(scores)

# run the experiment
for i in range(0, 3):
  reliability = i
  for j in range(0, 3):
    sensitivity = j
    for k in range(0, 3):
      always = k
      run_experiment(3)
f = open("lstm_only_results.txt", "w")
f.write(linesep.join(logs))
f.close()

# loaded_test_X, loaded_test_y = load_size_with_attempt(100, 1)
# test_X, test_y = prepare_data(loaded_test_X, loaded_test_y)
# print(loaded_test_X, loaded_test_y)
# print(test_X, test_y)
# print(test_X.shape)
