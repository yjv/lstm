import os.path
import time
import gc
from os import linesep
from numpy import mean
from numpy import std
from numpy import dstack
from numpy import swapaxes
from numpy import subtract
from pandas import read_csv
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers import LSTM
from keras.utils import to_categorical
from matplotlib import pyplot

reliability, sensitivity, always = 0, 0, 0
configuration = [
  # [50, 0, 50],
  # [50, 0, 100],
  # [50, 0, 200],
  # [50, 0.5, 50],
  # [50, 0.5, 100],
  # [50, 0.5, 200],
  # [50, 1, 50],
  # [50, 1, 100],
  # [50, 1, 200],
  # [100, 0, 50],
  # [100, 0, 100],
  # [100, 0, 200],
  # [100, 0.5, 50],
  # [100, 0.5, 100],
  # [100, 0.5, 200],
  # [100, 1, 50],
  # [100, 1, 100],
  # [100, 1, 200],
  # [200, 0, 50],
  # [200, 0, 100],
  # [200, 0, 200],
  # [200, 0.5, 50],
  # [200, 0.5, 100],
  # [200, 0.5, 200],
  # [200, 1, 50],
  # [200, 1, 100],
  # [200, 1, 200],
]
config = configuration[0]

def load_file(filepath):

  if os.path.isfile(filepath):
    # read your files
    dataframe = read_csv(filepath, header=None, delim_whitespace=False)
    return dataframe.values
  else:
    return []

def current_set():
  global reliability, sensitivity, always;
  return '_r' + str(reliability) + '_s' + str(sensitivity) + '_a' + str(always);

def load_size_with_attempt(size, attempt):
  prefix = '../logs_data/sample' + str(size).zfill(3) + current_set() + '/'
  loadedX = list()
  loadedy = list()
  for i in range(size * 2):
    for sensor in range(1, 6):
      filenameX = 'sensor' + str(sensor) + '_size_' + str(size).zfill(3) + '_attempt_' + str(attempt).zfill(3) + '_' + str(i + 1).zfill(3) + '.logs'
      dataX = load_file(prefix + filenameX)
      if (len(dataX)):
        loadedX.append(dataX)
        loadedy.append(sensor)
  return loadedX, loadedy

def load_size_with_all_attempt(size):
  loadedX = list()
  loadedy = list()
  prefix = '../logs_data/sample' + str(size).zfill(3) + current_set() + '/'
  for attempt in range(1, 11):
    for i in range(size * 2):
      for sensor in range(1, 5):
        filenameX = 'sensor' + str(sensor) + '_size_' + str(size).zfill(3) + '_attempt_' + str(attempt).zfill(3) + '_' + str(i + 1).zfill(3) + '.logs'
        dataX = load_file(prefix + filenameX)
        if (len(dataX)):
          loadedX.append(dataX)
          loadedy.append(sensor)
  return loadedX, loadedy

def prepare_data(X, y):
  X = swapaxes(X, 0, 0)
  y = to_categorical(subtract(y, 1), num_classes=5)
  return X, y

def load_dataset():
  loaded_X, loaded_y = load_size_with_attempt(250, 1)
  train_X, train_y = prepare_data(loaded_X, loaded_y)

  loaded_test_X, loaded_test_y = load_size_with_attempt(100, 1)
  test_X, test_y = prepare_data(loaded_test_X, loaded_test_y)

  return train_X, train_y, test_X, test_y

def evaluate_model(r, train_X, train_y, test_X, test_y):
  global config
  verbose, epochs, batch_size, validation_split = 0, 3, 64, 0.2
  # n_timesteps, n_features, n_outputs = train_X.shape[1], train_X.shape[2], train_y.shape[1]
  n_timesteps, n_features, n_outputs = 50, 9, 5
  model = Sequential()
  model.add(LSTM(config[0], input_shape=(n_timesteps,n_features)))
  if (config[1]):
    model.add(Dropout(config[1]))
  model.add(Dense(config[2], activation='relu'))
  model.add(Dense(n_outputs, activation='softmax'))
  model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
  # fit network
  model.fit(train_X, train_y, validation_split=validation_split, epochs=epochs, batch_size=batch_size, verbose=verbose)

  # serialize model to YAML
  model_yaml = model.to_yaml()
  with open("outputs/model" + str(r + 1) + ".yaml", "w") as yaml_file:
      yaml_file.write(model_yaml)
  # serialize weights to HDF5
  model.save_weights("outputs/model" + str(r + 1) + ".h5")

  # evaluate model
  _, accuracy = model.evaluate(test_X, test_y, batch_size=batch_size, verbose=verbose)
  return accuracy

def summarize_results(scores):
  print(scores)
  m, s = mean(scores), std(scores)
  print('Accuracy: %.3f%% (+/-%.3f)' % (m, s))

logs = list()
def run_experiment(train_X, train_y, test_X, test_y, repeats=1):
  global config
  # repeat experiment
  scores = list()
  global logs
  for r in range(repeats):
    score = evaluate_model(r, train_X, train_y, test_X, test_y)
    score = score * 100.0
    print(("lstm%d_dropout%d_dense%d.txt" % (config[0], config[1], config[2])) + 'set' + current_set() + '#%d: %.3f' % (r+1, score))
    scores.append(score)
    logs.append(','.join([str(reliability), str(sensitivity), str(always), '%.3f' % score]))
  # summarize results
  summarize_results(scores)

# run the experiment
used_time = 0;

train_X, train_y, test_X, test_y = load_dataset()

for x in range(len(configuration)):
  config = configuration[x]
  for i in range(0, 3):
    gc.collect()
    reliability = i
    for j in range(0, 3):
      sensitivity = j
      for k in range(0, 3):
        always = k
        start_time = time.time()
        run_experiment(train_X, train_y, test_X, test_y, 3)
        used_time += (time.time() - start_time)
        set_left = 27 * 27 - (x * 27) - (i * 9) - (j * 3) - k
        estimated_time = used_time + (set_left * (time.time() - start_time))
        print("%s / %s" % (used_time, estimated_time))
  f = open("lstm%d_dropout%d_dense%d.txt" % (config[0], config[1], config[2]), "w")
  f.write(linesep.join(logs))
  f.close()
