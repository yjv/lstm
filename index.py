from pandas import read_csv

def load_file(filepath):
  dataframe = read_csv(filepath, header=None, delim_whitespace=False)
  return dataframe.values

def load_sample(sample):
  result = [];
  for attempt_index in range(10):
    filename = './compiled_logs/size_' + str(sample).zfill(4) + '_attempt_' + str(attempt_index + 1).zfill(3) + '.csv';
    # result = result + [filename]
    result = result + [load_file(filename)]
  return result

sample = [10, 100, 250];
for i in range(len(sample)):
  print(*load_sample(sample[i]), sep = "\n")
